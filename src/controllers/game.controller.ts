import { Request, Response, NextFunction } from 'express';
import { BaseRoute } from '../routes';
import { GameService } from '../services';
import { BrowserMiddleware } from '../middlewares';

export class GameController extends BaseRoute {
    private controllerName: string = 'game';

    constructor() {
        super();
        this._initializeRoutes();
    }

    public _initializeRoutes() {
        this.router.post(`${this.path}/${this.controllerName}/create`, this.create);
        this.router.use(BrowserMiddleware);
        this.router.get(`${this.path}/${this.controllerName}/load`, this.load);
    }

    public async create(req: Request, res: Response, next: NextFunction) {
        try {
            const gameResponse = await GameService.createGame();
            return res.json(gameResponse);
        } catch (error) {
            return res.status(500).json({ message: error.message });
        }
    }

    public async load(req: any, res: Response, next: NextFunction) {
        try {
            const response = await GameService.loadGame(req.game.id);
            return res.json(response);
        } catch (error) {
            return res.status(500).json({ message: error.message });
        }
    }
}
