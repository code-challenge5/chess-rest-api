import { Response, NextFunction } from 'express';
import { BaseRoute } from '../routes';
import { BoardService, ChessService } from '../services';
import { BrowserMiddleware, BoardMiddleware } from '../middlewares';
import { gamePieces, movement, playerTypes } from '../types';

export class BoardController extends BaseRoute {
    private controllerName: string = 'board';

    constructor() {
        super();
        this._initializeRoutes();
    }

    public _initializeRoutes() {
        this.router.use(BrowserMiddleware);
        this.router.get(`${this.path}/${this.controllerName}/moves/legal/:position`, this.getLegalMoves);
        this.router.get(`${this.path}/${this.controllerName}/moves/create/:from/:to`, BoardMiddleware, this.createMoves);
        this.router.get(`${this.path}/${this.controllerName}/moves/history`, this.getMovesHistory)
    }

    public async getLegalMoves(req: any, res: Response) {
        try {
            const { position } = req.params;
            const lastBoardState = await BoardService.getLastBoardState(req.game.id);
            const chessInstance = new ChessService(JSON.parse(lastBoardState.board));
            const square = chessInstance.getBoard()[position];

            if (!square) {
                return res.status(400).json({ error: 'Invalid position!' });
            }

            if (square && square.piece !== gamePieces.PAWN) {
                return res.status(400).json({ error: 'Not a valid pawn move!' });
            }

            const moves = chessInstance.getLegalMoves(square.color, square.piece, position);
            return res.json(moves);
        } catch (error) {
            return res.status(500).json({ error: 'Something unexpected happened' });
        }
    }

    public async createMoves(req: any, res: Response, next: NextFunction) {
        try {
            const { from, to } = req.params;
            const gameId = req.game.id;
            const chessInstance = new ChessService(req.board);
            const moveFromBoardDetails = req.board[from];
            const moveToBoardDetails = req.board[to];

            if (!moveFromBoardDetails || !moveToBoardDetails) {
                return res.status(400).json({ error: 'Invalid position!' });
            }

            if (moveFromBoardDetails && moveFromBoardDetails.piece !== gamePieces.PAWN) {
                return res.status(400).json({ error: 'Not a valid pawn move!' });
            }

            const attemptMove = chessInstance.makeMove(moveFromBoardDetails.color, from, to);
            if (!attemptMove) {
                return res.status(400).json({ error: 'Invalid move!' });
            }

            const lastMove = await BoardService.getLastMove(gameId);
            const { moveNumber, turnPlayer, score } = req.boardModel;
            if (lastMove && lastMove.player === moveFromBoardDetails.color) {
                return res.status(400).json({ error: 'Invalid player turn!' });
            }

            const previousScoreDetails = JSON.parse(score);
            if (attemptMove === movement.ATTACK) {
                previousScoreDetails[moveFromBoardDetails.color][moveFromBoardDetails.piece] += 1;
            }

            const newBoardInstance = await BoardService.createNewBoard({
                gameId: gameId,
                moveNumber: moveNumber + 1,
                turnPlayer: turnPlayer && turnPlayer === playerTypes.WHITE ? playerTypes.BLACK : playerTypes.WHITE,
                score: JSON.stringify(previousScoreDetails),
                board: JSON.stringify(chessInstance.getBoard()),
            });

            const newMoveInstance = await BoardService.createNewMove({
                gameId: gameId,
                player: newBoardInstance.turnPlayer,
                moveNumber: newBoardInstance.moveNumber,
                from: from,
                to: to,
                piece: moveFromBoardDetails.piece,
                flag: attemptMove,
            });

            return res.json({ board: newBoardInstance, move: newMoveInstance });
        } catch (error) {
            return res.status(500).json({ error: 'Something unexpected happened' });
        }
    }

    public async getMovesHistory(req: any, res: Response, next: NextFunction) {
        return res.status(204).json({ message: 'No records', data: [] });
    }
}
