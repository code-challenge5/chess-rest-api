import { BaseRoute } from '../routes';
import { Response } from 'express';
import swaggerUi from 'swagger-ui-express';
import { swaggerDocument } from '../docs/swagger';

export class AppController extends BaseRoute {
    constructor() {
        super();
        this._initializeRoutes();
    }

    public _initializeRoutes() {
        this.router.get('/', this.initApp);
        this.router.use("/docs", swaggerUi.serve);
		this.router.get("/docs", swaggerUi.setup(swaggerDocument));
    }

    public initApp(req: any, res: Response) {
        return res.json({
            message: 'Welcome to Chess API',
        });
    }
}
