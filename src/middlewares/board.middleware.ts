import { Response, NextFunction } from 'express';
import { Board } from '../models'
import { ChessService } from '../services';

export const BoardMiddleware = async (req: any, res: Response, next: NextFunction) => {
    const existingBoard = await Board.findOne({ gameId: req.game.id }).sort({ _id: -1 });
    const chessInstance = new ChessService(JSON.parse(existingBoard.board));

    req.board = chessInstance.getBoard();
    req.boardModel = existingBoard;
    next();
};
