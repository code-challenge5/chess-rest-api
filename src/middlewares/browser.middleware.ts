import { Response, NextFunction } from 'express';
import { Game } from '../models';

export const BrowserMiddleware = async (req: any, res: Response, next: NextFunction) => {
    const { browser_id } = req.headers;

    if (!browser_id) {
        return res.status(400).json({ error: 'No browser id' });
    }

    const existingGame = await Game.findOne({ browserId: browser_id });
    if (!existingGame) {
        return res.status(400).json({ error: 'Game not found!' });
    }

    req.game = existingGame;
    next();
};
