import { Board, BoardDocument, Move, MoveDocument } from '../models'

export class BoardService {
    public static async getLastBoardState(gameId: string): Promise<BoardDocument> {
        return await Board.findOne({ gameId }).sort({ _id: -1 });
    }

    public static async createNewBoard(board: any): Promise<BoardDocument> {
        return await Board.create(board);
    }

    public static async createNewMove(move: any): Promise<MoveDocument> {
        return await Move.create(move);
    }

    public static async getLastMove(gameId: string): Promise<MoveDocument> {
        return await Move.findOne({ gameId }).sort({ _id: -1 });
    }
}
