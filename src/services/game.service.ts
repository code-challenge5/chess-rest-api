import { Game, Board, Move } from '../models'
import { ChessService, BoardService } from '../services'
import * as bcrypt from 'bcrypt';

export class GameService {
    public static async createGame(): Promise<any> {
        const encryptedBrowswerId = await bcrypt.hash(new Date().getTime().toString(), 8);
        const newGameInstance = await Game.create({
            browserId: encryptedBrowswerId,
        });
        const chessInstance = new ChessService();
        const newBoardInstance = await BoardService.createNewBoard({
            gameId: newGameInstance._id,
            board: JSON.stringify(chessInstance.getBoard()),
            score: JSON.stringify(chessInstance.initializeBoardScore()),
            moveNumber: 0,
            turnPlayer: ''
        });

        return { game: newGameInstance, board: newBoardInstance };
    }

    public static async loadGame(gameId: string): Promise<any> {
        const existingBoard = await Board.findOne({ gameId }).sort({ _id: -1 });
        const existingMoves = await Move.find({ gameId }).sort({ _id: -1 });

        return { board: existingBoard, moves: existingMoves };
    }
}
