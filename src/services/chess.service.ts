import { BAORD_SQUARES, playerTypes, gamePieces, PAWN_OFFSETS, SECOND_RANK, movement, validPieces } from '../types';
import { BoardDocument } from '../models'

export class ChessService {
    private board;

    constructor(board?: BoardDocument) {
        if (board) {
            this.board = board;
        } else {
            this.resetBoard();
        }
    }

    public initializeBoardScore() {
        const pieces = {};
        const score = {};

        pieces[gamePieces.PAWN] = 0;
        pieces[gamePieces.KNIGHT] = 0;
        pieces[gamePieces.BISHOP] = 0;
        pieces[gamePieces.ROOK] = 0;
        pieces[gamePieces.QUEEN] = 0;
        pieces[gamePieces.KING] = 0;
        
        score[playerTypes.WHITE] = pieces;
        score[playerTypes.BLACK] = pieces;

        return score;
    }

    public resetBoard() {
        const boardSquares = Object.keys(BAORD_SQUARES);
        this.board = {};

        boardSquares.map((square) => {
            this.board[square] = {
                color: '',
                piece: null,
            };
            return square;
        });

        this.board.a8 = { color: playerTypes.BLACK, piece: gamePieces.ROOK };
        this.board.b8 = { color: playerTypes.BLACK, piece: gamePieces.KNIGHT };
        this.board.c8 = { color: playerTypes.BLACK, piece: gamePieces.BISHOP };
        this.board.d8 = { color: playerTypes.BLACK, piece: gamePieces.QUEEN };
        this.board.e8 = { color: playerTypes.BLACK, piece: gamePieces.KING };
        this.board.f8 = { color: playerTypes.BLACK, piece: gamePieces.BISHOP };
        this.board.g8 = { color: playerTypes.BLACK, piece: gamePieces.KNIGHT };
        this.board.h8 = { color: playerTypes.BLACK, piece: gamePieces.ROOK };

        this.board.a7 = { color: playerTypes.BLACK, piece: gamePieces.PAWN };
        this.board.b7 = { color: playerTypes.BLACK, piece: gamePieces.PAWN };
        this.board.c7 = { color: playerTypes.BLACK, piece: gamePieces.PAWN };
        this.board.d7 = { color: playerTypes.BLACK, piece: gamePieces.PAWN };
        this.board.e7 = { color: playerTypes.BLACK, piece: gamePieces.PAWN };
        this.board.f7 = { color: playerTypes.BLACK, piece: gamePieces.PAWN };
        this.board.g7 = { color: playerTypes.BLACK, piece: gamePieces.PAWN };
        this.board.h7 = { color: playerTypes.BLACK, piece: gamePieces.PAWN };

        this.board.a1 = { color: playerTypes.WHITE, piece: gamePieces.ROOK };
        this.board.b1 = { color: playerTypes.WHITE, piece: gamePieces.KNIGHT };
        this.board.c1 = { color: playerTypes.WHITE, piece: gamePieces.BISHOP };
        this.board.d1 = { color: playerTypes.WHITE, piece: gamePieces.QUEEN };
        this.board.e1 = { color: playerTypes.WHITE, piece: gamePieces.KING };
        this.board.f1 = { color: playerTypes.WHITE, piece: gamePieces.BISHOP };
        this.board.g1 = { color: playerTypes.WHITE, piece: gamePieces.KNIGHT };
        this.board.h1 = { color: playerTypes.WHITE, piece: gamePieces.ROOK };

        this.board.a2 = { color: playerTypes.WHITE, piece: gamePieces.PAWN };
        this.board.b2 = { color: playerTypes.WHITE, piece: gamePieces.PAWN };
        this.board.c2 = { color: playerTypes.WHITE, piece: gamePieces.PAWN };
        this.board.d2 = { color: playerTypes.WHITE, piece: gamePieces.PAWN };
        this.board.e2 = { color: playerTypes.WHITE, piece: gamePieces.PAWN };
        this.board.f2 = { color: playerTypes.WHITE, piece: gamePieces.PAWN };
        this.board.g2 = { color: playerTypes.WHITE, piece: gamePieces.PAWN };
        this.board.h2 = { color: playerTypes.WHITE, piece: gamePieces.PAWN };
    }

    public getBoard() {
        return this.board;
    }

    public setBoard(board) {
        this.board = board;
    }

    private convertValueToSAN(value) {
        const swaped = Object.fromEntries(Object.entries(BAORD_SQUARES).map((a) => a.reverse()));
        return swaped[value];
    }

    private getRank(position) {
        if (typeof position === 'string') {
            return position[1];
        }
        return this.convertValueToSAN(position)[1];
    }

    private isPositionInsideBoard(position) {
        if (typeof position === 'string') {
            position = BAORD_SQUARES[`${position}`.toLowerCase()];
            if (position === undefined) {
                return null;
            }
        }

        const lastPosition = BAORD_SQUARES.h1;
        const firstPosition = BAORD_SQUARES.a8;

        if (position >= firstPosition && position <= lastPosition && (position & 0x88) === 0) {
            return position;
        }

        return null;
    }

    public canPawnDoubleJump(color, position): boolean {
        if (typeof position === 'string') {
            position = BAORD_SQUARES[position];
        }

        const rank = this.getRank(position);
        if (rank === SECOND_RANK[color]) {
            const offsets = PAWN_OFFSETS[color];
            const move = offsets[1] + position;
            let san = this.convertValueToSAN(move);
            const square = this.board[san];

            const front = offsets[0] + position;
            san = this.convertValueToSAN(front);
            const frontSquare = this.board[san];

            if (square && frontSquare) {
                if (!square.piece && !frontSquare.piece) {
                    return true;
                }
            }
        }

        return false;
    }

    private validateMove(color, piece, from, to) {
        const legalMoves = this.getLegalMoves(color, piece, from);
        return legalMoves.map(move => move.move).includes(to);
    }

    public makeMove(color, from, to) {
        const square = this.board[from];
        if (square.color && square.color === color) {
            if (this.validateMove(color, square.piece, from, to)) {
                let flag = movement.NORMAL;

                if (this.board[to].color) {
                    if (this.board[to].color !== color) {
                        flag = movement.ATTACK;
                    }
                }

                this.board[to] = { color: color, piece: square.piece };
                this.board[from] = { color: '', piece: null };
                return flag;
            }
        }

        return null;
    }

    public getLegalMoves(color, piece, position) {
        const positionValue = this.isPositionInsideBoard(position);
        const legalMoves = [];

        if (positionValue !== null && validPieces.includes(piece)) {
            /**
             * Pawn movement only
             */
            if (piece === gamePieces.PAWN) {
                const offsets = PAWN_OFFSETS[color];
                const pawnPossibleMoves = [];

                // normal movement
                pawnPossibleMoves.push(offsets[0] + positionValue);

                // double jump
                if (this.canPawnDoubleJump(color, positionValue)) {
                    pawnPossibleMoves.push(offsets[1] + positionValue);
                }

                for (let i = 0; i < pawnPossibleMoves.length; i++) {
                    const move = pawnPossibleMoves[i];
                    if (this.isPositionInsideBoard(move)) {
                        const san = this.convertValueToSAN(move);
                        const square = this.board[san];

                        if (!square.piece) {
                            legalMoves.push({
                                move: san,
                                capture: false
                            });
                        }
                    }
                }

                for (let i = 2; i < 4; i++) {
                    const pawnAttack = offsets[i] + positionValue;
                    if (this.isPositionInsideBoard(pawnAttack)) {
                        const san = this.convertValueToSAN(pawnAttack);
                        const square = this.board[san];

                        if (square.piece && square.color !== color) {
                            legalMoves.push({
                                move: san,
                                capture: true
                            });
                        }
                    }
                }
            } else {
                /**
                 * Every other piece movement comes here
                 */
            }
        }

        return legalMoves;
    }
}
