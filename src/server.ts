import dotenv from 'dotenv';
import { App } from './index';
import { GameController, BoardController, AppController } from './controllers';

dotenv.config();

const port = 8000;
const app = new App([
    new AppController(), 
    new GameController(), 
    new BoardController()
], port);

app.listen();
