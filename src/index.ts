import express from 'express';
import mongoose from 'mongoose';
import cors from 'cors';
import morgan from 'morgan';
import helmet from 'helmet';
import Youch from 'youch';
import { log } from './utils';

export class App {
    public app: any;
    public port: number;

    constructor(controllers: Array<any>, port: number) {
        this.app = express();
        this.port = port;
        this.initializeExceptionHandler();
        this.initializeDatabase();
        this.initializeMiddlewares();
        this.initializeControllers(controllers);
    }

    private initializeMiddlewares() {
        this.app.use(helmet());
        this.app.use(cors());
        this.app.use(morgan('dev'));
        this.app.use(express.json());
        this.app.use(express.urlencoded({ extended: false }));
    }

    private initializeControllers(controllers) {
        for (const controller of controllers) {
            this.app.use('/', controller.router);
        }
    }

    private async initializeDatabase() {
        try {
            await mongoose.connect(process.env.NODE_ENV === 'production' ? 'mongodb://mongo:27017/chessApiDB' : process.env.MONGO_DB_URL, {
                useNewUrlParser: true,
                useUnifiedTopology: true,
            });

            log.info('Database connected');
        } catch (error) {
            log.error(error.message);
        }
    }

    private async initializeExceptionHandler() {
        this.app.use(async (err, req, res, next) => {
            if (process.env.NODE_ENV === 'development') {
                const errors = await new Youch(err, req).toJSON();
                return res.status(err.status || 500).json(errors);
            }

            return res.status(err.status || 500).json({ error: 'Internal server error.' });
        });
    }

    public listen() {
        this.app.listen(this.port, () => {
            log.info('App is running on http://localhost:' + this.port);
        });
    }
}
