export const swaggerDocument = {
    openapi: '3.0.1',
    info: {
        version: '1.0.0',
        title: 'Chess RESTful API',
        description: '',
    },
    tags: [{ name: 'Games' }, { name: 'Board' }],
    paths: {
        '/api/game/create': {
            post: {
                tags: ['Games'],
                description: 'This endpoint is used to create a new game.',
                requestBody: {
                    description: '',
                    required: false,
                    schema: {},
                    example: {}
                },
                responses: {
                    '200': {
                        content: {
                            'application/json': {
                                schema: {},
                                example: {
                                    game: {
                                        browserId: '$2b$08$W7TDfJdvI8rl0paqRCsKPOhefl6oab6owaBLGCH5453g3eaNDJAMW',
                                        createdAt: '2021-08-09T16:21:06.021Z',
                                        updatedAt: '2021-08-09T16:21:06.021Z',
                                        id: '611155f2612e658151848dce',
                                    },
                                    board: {
                                        board: '{"a8":{"color":"B","piece":"R"},"b8":{"color":"B","piece":"N"},"c8":{"color":"B","piece":"B"},"d8":{"color":"B","piece":"Q"},"e8":{"color":"B","piece":"K"},"f8":{"color":"B","piece":"B"},"g8":{"color":"B","piece":"N"},"h8":{"color":"B","piece":"R"},"a7":{"color":"B","piece":"P"},"b7":{"color":"B","piece":"P"},"c7":{"color":"B","piece":"P"},"d7":{"color":"B","piece":"P"},"e7":{"color":"B","piece":"P"},"f7":{"color":"B","piece":"P"},"g7":{"color":"B","piece":"P"},"h7":{"color":"B","piece":"P"},"a6":{"color":"","piece":null},"b6":{"color":"","piece":null},"c6":{"color":"","piece":null},"d6":{"color":"","piece":null},"e6":{"color":"","piece":null},"f6":{"color":"","piece":null},"g6":{"color":"","piece":null},"h6":{"color":"","piece":null},"a5":{"color":"","piece":null},"b5":{"color":"","piece":null},"c5":{"color":"","piece":null},"d5":{"color":"","piece":null},"e5":{"color":"","piece":null},"f5":{"color":"","piece":null},"g5":{"color":"","piece":null},"h5":{"color":"","piece":null},"a4":{"color":"","piece":null},"b4":{"color":"","piece":null},"c4":{"color":"","piece":null},"d4":{"color":"","piece":null},"e4":{"color":"","piece":null},"f4":{"color":"","piece":null},"g4":{"color":"","piece":null},"h4":{"color":"","piece":null},"a3":{"color":"","piece":null},"b3":{"color":"","piece":null},"c3":{"color":"","piece":null},"d3":{"color":"W","piece":"P"},"e3":{"color":"","piece":null},"f3":{"color":"","piece":null},"g3":{"color":"","piece":null},"h3":{"color":"","piece":null},"a2":{"color":"W","piece":"P"},"b2":{"color":"W","piece":"P"},"c2":{"color":"W","piece":"P"},"d2":{"color":"","piece":null},"e2":{"color":"W","piece":"P"},"f2":{"color":"W","piece":"P"},"g2":{"color":"W","piece":"P"},"h2":{"color":"W","piece":"P"},"a1":{"color":"W","piece":"R"},"b1":{"color":"W","piece":"N"},"c1":{"color":"W","piece":"B"},"d1":{"color":"W","piece":"Q"},"e1":{"color":"W","piece":"K"},"f1":{"color":"W","piece":"B"},"g1":{"color":"W","piece":"N"},"h1":{"color":"W","piece":"R"}}',
                                        score: '{"W":{"P":0,"N":0,"B":0,"R":0,"Q":0,"K":0},"B":{"P":0,"N":0,"B":0,"R":0,"Q":0,"K":0}}',
                                        turnPlayer: '',
                                        gameId: '611155f2612e658151848dce',
                                        moveNumber: 0,
                                        createdAt: '2021-08-09T16:21:06.036Z',
                                        updatedAt: '2021-08-09T16:21:06.036Z',
                                        id: '611155f2612e658151848dd0',
                                    },
                                },
                            },
                        },
                    },
                    '500': {
                        description: 'Something unexpected happened',
                        content: {
                            'application/json': {
                                example: {
                                    message: 'Error message',
                                },
                            },
                        },
                    },
                },
            },
        },
        '/api/game/load': {
            parameters: [
                {
                    name: 'browserId',
                    in: 'header',
                    required: true,
                    description: 'A valid browser ID from a valid game instance',
                    schema: {
                        type: 'string'
                    }
                },
            ],
            get: {
                tags: ['Games'],
                description: 'Load a game instance',
                responses: {
                    '200': {
                        description: 'Loaded game instance details',
                        content: {
                            'application/json': {
                                schema: {},
                                example: {
                                    board: {
                                        board: {
                                            board: '{"a8":{"color":"B","piece":"R"},"b8":{"color":"B","piece":"N"},"c8":{"color":"B","piece":"B"},"d8":{"color":"B","piece":"Q"},"e8":{"color":"B","piece":"K"},"f8":{"color":"B","piece":"B"},"g8":{"color":"B","piece":"N"},"h8":{"color":"B","piece":"R"},"a7":{"color":"B","piece":"P"},"b7":{"color":"B","piece":"P"},"c7":{"color":"B","piece":"P"},"d7":{"color":"B","piece":"P"},"e7":{"color":"B","piece":"P"},"f7":{"color":"B","piece":"P"},"g7":{"color":"B","piece":"P"},"h7":{"color":"B","piece":"P"},"a6":{"color":"","piece":null},"b6":{"color":"","piece":null},"c6":{"color":"","piece":null},"d6":{"color":"","piece":null},"e6":{"color":"","piece":null},"f6":{"color":"","piece":null},"g6":{"color":"","piece":null},"h6":{"color":"","piece":null},"a5":{"color":"","piece":null},"b5":{"color":"","piece":null},"c5":{"color":"","piece":null},"d5":{"color":"","piece":null},"e5":{"color":"","piece":null},"f5":{"color":"","piece":null},"g5":{"color":"","piece":null},"h5":{"color":"","piece":null},"a4":{"color":"","piece":null},"b4":{"color":"","piece":null},"c4":{"color":"","piece":null},"d4":{"color":"","piece":null},"e4":{"color":"","piece":null},"f4":{"color":"","piece":null},"g4":{"color":"","piece":null},"h4":{"color":"","piece":null},"a3":{"color":"","piece":null},"b3":{"color":"","piece":null},"c3":{"color":"","piece":null},"d3":{"color":"","piece":null},"e3":{"color":"","piece":null},"f3":{"color":"","piece":null},"g3":{"color":"","piece":null},"h3":{"color":"","piece":null},"a2":{"color":"W","piece":"P"},"b2":{"color":"W","piece":"P"},"c2":{"color":"W","piece":"P"},"d2":{"color":"W","piece":"P"},"e2":{"color":"W","piece":"P"},"f2":{"color":"W","piece":"P"},"g2":{"color":"W","piece":"P"},"h2":{"color":"W","piece":"P"},"a1":{"color":"W","piece":"R"},"b1":{"color":"W","piece":"N"},"c1":{"color":"W","piece":"B"},"d1":{"color":"W","piece":"Q"},"e1":{"color":"W","piece":"K"},"f1":{"color":"W","piece":"B"},"g1":{"color":"W","piece":"N"},"h1":{"color":"W","piece":"R"}}',
                                            score: '{"W":{"P":0,"N":0,"B":0,"R":0,"Q":0,"K":0},"B":{"P":0,"N":0,"B":0,"R":0,"Q":0,"K":0}}',
                                            turnPlayer: '',
                                            gameId: '611155f2612e658151848dce',
                                            moveNumber: 0,
                                            createdAt: '2021-08-09T16:21:06.036Z',
                                            updatedAt: '2021-08-09T16:21:06.036Z',
                                            id: '611155f2612e658151848dd0',
                                        },
                                    },
                                    moves: [],
                                },
                            },
                        },
                    },
                    '500': {
                        description: 'Something unexpected happened',
                        content: {
                            'application/json': {
                                example: {
                                    message: 'Error message',
                                },
                            },
                        },
                    },
                },
            },
        },
        '/api/board/moves/legal/{position}': {
            parameters: [
                {
                    name: 'position',
                    in: 'path',
                    required: true,
                    description: 'A possible position identifier',
                    schema: {
                        type: 'string'
                    }
                },
            ],
            get: {
                tags: ['Board'],
                description: 'This endpoint can be used to determine where any piece – white or black – can possibly move from its current position.',
                responses: {
                    '200': {
                        content: {
                            'application/json': {
                                schema: {},
                                example: [
                                    {
                                        move: 'd3',
                                        capture: false,
                                    },
                                    {
                                        move: 'd4',
                                        capture: false,
                                    },
                                ],
                            },
                        },
                    },
                    '400': {
                        description: 'Invalid request',
                        content: {
                            'application/json': {
                                example: {
                                    message: 'Invalid position!',
                                },
                            },
                        },
                    },
                    '500': {
                        description: 'Server error',
                        content: {
                            'application/json': {
                                example: {
                                    message: 'Something unexpected happened',
                                },
                            },
                        },
                    },
                },
            },
        },
        '/api/board/moves/create/{from}/{to}': {
            parameters: [
                {
                    name: 'from',
                    in: 'path',
                    required: true,
                    description: 'The current position to move from',
                    schema: {
                        type: 'string'
                    }
                },
                {
                    name: 'to',
                    in: 'path',
                    required: true,
                    description: 'The target position to move to',
                    schema: {
                        type: 'string'
                    }
                },
            ],
            get: {
                tags: ['Board'],
                description: 'This endpoint will be used by clients to attempt to make a move',
                responses: {
                    '200': {
                        content: {
                            'application/json': {
                                schema: {},
                                example: {
                                    board: {
                                        board: '{"a8":{"color":"B","piece":"R"},"b8":{"color":"B","piece":"N"},"c8":{"color":"B","piece":"B"},"d8":{"color":"B","piece":"Q"},"e8":{"color":"B","piece":"K"},"f8":{"color":"B","piece":"B"},"g8":{"color":"B","piece":"N"},"h8":{"color":"B","piece":"R"},"a7":{"color":"B","piece":"P"},"b7":{"color":"B","piece":"P"},"c7":{"color":"B","piece":"P"},"d7":{"color":"B","piece":"P"},"e7":{"color":"B","piece":"P"},"f7":{"color":"B","piece":"P"},"g7":{"color":"B","piece":"P"},"h7":{"color":"B","piece":"P"},"a6":{"color":"","piece":null},"b6":{"color":"","piece":null},"c6":{"color":"","piece":null},"d6":{"color":"","piece":null},"e6":{"color":"","piece":null},"f6":{"color":"","piece":null},"g6":{"color":"","piece":null},"h6":{"color":"","piece":null},"a5":{"color":"","piece":null},"b5":{"color":"","piece":null},"c5":{"color":"","piece":null},"d5":{"color":"","piece":null},"e5":{"color":"","piece":null},"f5":{"color":"","piece":null},"g5":{"color":"","piece":null},"h5":{"color":"","piece":null},"a4":{"color":"","piece":null},"b4":{"color":"","piece":null},"c4":{"color":"","piece":null},"d4":{"color":"","piece":null},"e4":{"color":"","piece":null},"f4":{"color":"","piece":null},"g4":{"color":"","piece":null},"h4":{"color":"","piece":null},"a3":{"color":"","piece":null},"b3":{"color":"","piece":null},"c3":{"color":"","piece":null},"d3":{"color":"W","piece":"P"},"e3":{"color":"","piece":null},"f3":{"color":"","piece":null},"g3":{"color":"","piece":null},"h3":{"color":"","piece":null},"a2":{"color":"W","piece":"P"},"b2":{"color":"W","piece":"P"},"c2":{"color":"W","piece":"P"},"d2":{"color":"","piece":null},"e2":{"color":"W","piece":"P"},"f2":{"color":"W","piece":"P"},"g2":{"color":"W","piece":"P"},"h2":{"color":"W","piece":"P"},"a1":{"color":"W","piece":"R"},"b1":{"color":"W","piece":"N"},"c1":{"color":"W","piece":"B"},"d1":{"color":"W","piece":"Q"},"e1":{"color":"W","piece":"K"},"f1":{"color":"W","piece":"B"},"g1":{"color":"W","piece":"N"},"h1":{"color":"W","piece":"R"}}',
                                        score: '{"W":{"P":0,"N":0,"B":0,"R":0,"Q":0,"K":0},"B":{"P":0,"N":0,"B":0,"R":0,"Q":0,"K":0}}',
                                        turnPlayer: 'W',
                                        gameId: '611155f2612e658151848dce',
                                        moveNumber: 1,
                                        createdAt: '2021-08-09T19:22:56.036Z',
                                        updatedAt: '2021-08-09T19:22:56.036Z',
                                        id: '611180908af0c5202f8fbd64',
                                    },
                                    move: {
                                        player: 'W',
                                        from: 'd2',
                                        to: 'd3',
                                        piece: 'P',
                                        flag: '-',
                                        gameId: '611155f2612e658151848dce',
                                        moveNumber: 1,
                                        createdAt: '2021-08-09T19:22:56.043Z',
                                        updatedAt: '2021-08-09T19:22:56.043Z',
                                        id: '611180908af0c5202f8fbd66',
                                    },
                                },
                            },
                        },
                    },
                    '400': {
                        description: 'Not a valid pawn move',
                        content: {
                            'application/json': {
                                example: {
                                    message: 'Not a valid pawn move!',
                                },
                            },
                        },
                    },
                    '500': {
                        description: 'Something unexpected happened',
                        content: {
                            'application/json': {
                                example: {
                                    message: 'Something unexpected happened',
                                },
                            },
                        },
                    },
                },
            },
        },
        '/api/board/moves/history': {
            get: {
                tags: ['Board'],
                description: 'This endpoint is used to return the history of moves made by both sides.',
                responses: {
                    '204': {
                        content: {
                            'application/json': {
                                schema: {},
                                example: [],
                            },
                        },
                    },
                    '500': {
                        description: 'Something unexpected happened',
                        content: {
                            'application/json': {
                                example: {
                                    message: 'Something unexpected happened',
                                },
                            },
                        },
                    },
                },
            },
        },
    },
};
