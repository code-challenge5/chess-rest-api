import mongoose from 'mongoose';

export interface GameDocument extends mongoose.Document {
    id: string;
    browserId: string;
    createdAt: Date;
    updatedAt: Date;
}

const GameSchema = new mongoose.Schema(
    {
        browserId: { type: String, default: null },
    },
    {
        timestamps: true,
        toJSON: {
            virtuals: true,
            transform: (_doc: any, ret: any): void => {
                delete ret._id;
                delete ret.__v;
            },
        },
    }
);

export const Game = mongoose.model<GameDocument>('Game', GameSchema);
