import mongoose from 'mongoose';
import { GameDocument } from './game.model'

export interface MoveDocument extends mongoose.Document {
    id: string;
    gameId: GameDocument["_id"];
    moveNumber: number;
    player: string;
    from: string;
    to: string;
    piece: string;
    flag: string;
    createdAt: Date;
    updatedAt: Date;
}

const MoveSchema = new mongoose.Schema(
    {
        gameId: { type: mongoose.Schema.Types.ObjectId, ref: "Game" },
        moveNumber: { type: Number },
        player: { type: String, default: null },
        from: { type: String, default: null },
        to: { type: String, default: null },
        piece: { type: String, default: null },
        flag: { type: String, default: null }
    },
    {
        timestamps: true,
        toJSON: {
            virtuals: true,
            transform: (_doc: any, ret: any): void => {
                delete ret._id;
                delete ret.__v;
            },
        },
    }
);

export const Move = mongoose.model<MoveDocument>('Move', MoveSchema);
