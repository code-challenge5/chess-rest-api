import mongoose from 'mongoose';
import { GameDocument } from './game.model'

export interface BoardDocument extends mongoose.Document {
    id: string;
    gameId: GameDocument["_id"];
    board: string;
    score: string;
    moveNumber: number;
    turnPlayer: string;
    createdAt: Date;
    updatedAt: Date;
}

const BoardSchema = new mongoose.Schema(
    {
        gameId: { type: mongoose.Schema.Types.ObjectId, ref: "Game" },
        board: { type: String, default: null },
        score: { type: String, default: null },
        moveNumber: { type: Number },
        turnPlayer: { type: String, default: null }
    },
    {
        timestamps: true,
        toJSON: {
            virtuals: true,
            transform: (_doc: any, ret: any): void => {
                delete ret._id;
                delete ret.__v;
            },
        },
    }
);

export const Board = mongoose.model<BoardDocument>('Board', BoardSchema);
